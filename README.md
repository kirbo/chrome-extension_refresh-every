# Refresh Every -extension

Adds "Refresh Every" -submenu in Context Menu and toolbar, which allows user to set auto-refresh for current tab. Motivation behind this extension is the lack of this feature originally found in Legacy Opera browser.

## CI/CD example

- https://circleci.com/blog/continuously-deploy-a-chrome-extension

## Documentation Sources

- https://developer.chrome.com/extensions
- https://developer.chrome.com/extensions/browserAction
- https://developer.chrome.com/extensions/commands
- https://developer.chrome.com/extensions/contextMenus
- https://developer.chrome.com/extensions/pageAction
- https://developer.chrome.com/extensions/a11y
- https://developer.chrome.com/extensions/background_pages
- https://developer.chrome.com/extensions/i18n
- https://developer.chrome.com/extensions/options
- https://developer.chrome.com/extensions/permissions
- https://developer.chrome.com/extensions/storage
- https://developer.chrome.com/extensions/tabs
- https://developer.chrome.com/extensions/windows
- https://developer.chrome.com/extensions/activeTab
- https://developer.chrome.com/extensions/permission_warnings
