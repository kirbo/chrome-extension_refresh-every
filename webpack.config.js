const path = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { optimize } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

let prodPlugins = [];
let optimization;
if (process.env.NODE_ENV === 'production') {
  prodPlugins = [
    ...prodPlugins,
    new optimize.AggressiveMergingPlugin(),
    new optimize.OccurrenceOrderPlugin(),
  ];

  optimization = {
    minimize: true,
    usedExports: true,
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        terserOptions: {
          ecma: 2016,
          warnings: false,
          parse: {},
          compress: {},
          mangle: true,
          module: false,
          output: null,
          toplevel: false,
          nameCache: null,
          ie8: false,
          keep_classnames: undefined,
          keep_fnames: false,
          safari10: false,
        },
      }),
    ],
  };
}

module.exports = {
  mode: process.env.NODE_ENV || 'production',
  devtool: 'inline-source-map',
  entry: {
    backgroundScripts: path.resolve(__dirname, 'src/backgroundScripts.ts'),
    popup: path.resolve(__dirname, 'src/popup.tsx'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.tsx?$/,
        use: 'awesome-typescript-loader?{configFileName: "tsconfig.json"}',
      },
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },
  optimization,
  plugins: [
    new CheckerPlugin(),
    ...prodPlugins,
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new CopyPlugin(['src/static']),
    new HtmlWebpackPlugin({
      chunks: ['popup'],
      filename: 'popup.html',
      title: 'Refresh every',
      inject: true,
      template: 'src/assets/popup.html',
    }),
  ],
  resolve: {
    alias: {
      '@assets': path.resolve(__dirname, 'src/assets'),
      '@backgroundScripts': path.resolve(__dirname, 'src/backgroundScripts'),
      '@helpers': path.resolve(__dirname, 'src/helpers'),
      '@redux': path.resolve(__dirname, 'src/redux'),
      '@static': path.resolve(__dirname, 'src/static'),
      '@types': path.resolve(__dirname, 'src/types'),
      '@views': path.resolve(__dirname, 'src/views'),
    },
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
};
