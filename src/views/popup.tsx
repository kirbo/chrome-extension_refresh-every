import { RootState } from 'refresh-every';

import React from 'react';
import { connect, ConnectedComponent } from 'react-redux';

import { setInterval, removeInterval } from '@redux/intervals/actions';

import { getCurrentTab } from '@helpers/common';

const randomInteger = () => Math.round(Math.random() * 100);

const buttons = (row, setTabInterval, removeTabInterval) => (
  <>
    <button
      onClick={() => {
        const payload = {
          ...row,
          interval: randomInteger(),
        };
        setTabInterval(payload);
      }}
    >
      setInterval
    </button>
    <button
      onClick={() => {
        removeTabInterval(row);
      }}
    >
      removeInterval
    </button>
  </>
);

export const backgroundPage = ({
  store,
  addTabInterval,
  setTabInterval,
  removeTabInterval,
}) => (
  <div>
    Moi!
    <button
      onClick={async () => {
        const currentTab = await getCurrentTab();
        console.log(currentTab);

        const payload = {
          tab: currentTab.id,
          interval: randomInteger(),
        };
        setTabInterval(payload);
      }}
    >
      setInterval
    </button>
    <table>
      <thead>
        <tr>
          <th>Tab</th>
          <th>Interval</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {store.intervals.map(row => (
          <tr key={row.tab}>
            <td>{row.tab}</td>
            <td>{row.interval}</td>
            <td>{buttons(row, setTabInterval, removeTabInterval)}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

const smart = connect(
  (store: RootState) => ({
    store,
  }),
  dispatch => ({
    setTabInterval: (payload: any) => dispatch(setInterval(payload)),
    removeTabInterval: (payload: any) => dispatch(removeInterval(payload)),
  })
);

export default smart(backgroundPage) as ConnectedComponent<any, any>;
