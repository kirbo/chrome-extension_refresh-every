import { Actions, ScopedActions } from 'refresh-every';

const SCOPE_DELIMITER = '/';

export const scopeActions = (scope: string, actions: Actions): ScopedActions =>
  Object.keys(actions).reduce((scopedActions: ScopedActions, key: string) => {
    scopedActions[key] = `${scope}${SCOPE_DELIMITER}${actions[key]}`;
    return scopedActions;
  }, {});
