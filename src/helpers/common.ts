import {
  ParentItem,
  ParentId,
  ChildItem,
  MenuType,
  ConvertIdToTitle,
} from 'refresh-every';

import moment from 'moment';

import store from '@redux/store';

const background = chrome.extension.getBackgroundPage();

export const log = (...rest: any) => {
  background.console.log(...rest);
};

export const convertIdToTitle = ({ id, title, parentId }: ConvertIdToTitle) => {
  const modifiedId = parentId ? `${parentId}-${id || title}` : id || title;
  return modifiedId.replace(/ /g, '_');
};

export const addMenu = (menus: MenuType, parentId: ParentId = undefined) => {
  menus.forEach((thisMenuItem: ParentItem | ChildItem) => {
    const { id, title, contexts, onclick, type } = thisMenuItem;

    const menuItem = {
      id: convertIdToTitle({ id, title, parentId }),
      title,
      contexts,
      onclick,
      parentId,
    };

    const separatorItem = {
      ...thisMenuItem,
      parentId,
    };

    let contextMenuItem = type === 'separator' ? separatorItem : menuItem;

    chrome.contextMenus.create(contextMenuItem);

    if (thisMenuItem.menu) {
      addMenu(thisMenuItem.menu, menuItem.id);
    }
  });
};

export const getCurrentTab = () =>
  new Promise<chrome.tabs.Tab>((resolve, reject) => {
    const query = { active: true, currentWindow: true };
    const callback = (tabs: chrome.tabs.Tab[]) => {
      if (tabs && tabs[0]) {
        resolve(tabs[0]);
      } else {
        reject();
      }
    };
    chrome.tabs.query(query, callback);
  });

export const getTabIdsForUrl = (url: string) =>
  new Promise<chrome.tabs.Tab[]>((resolve, reject) => {
    const query = { url };
    const callback = (tabs: chrome.tabs.Tab[]) => {
      if (tabs) {
        resolve(tabs);
      } else {
        reject();
      }
    };
    chrome.tabs.query(query, callback);
  });

export const setBadge = (duration: number = 0) => {
  let content = '';

  if (duration > 0) {
    const difference = moment.duration(
      moment().add(duration, 'seconds').diff(moment())
    );

    const years = difference.years();
    const months = difference.months();
    const weeks = difference.weeks();
    const days = difference.days();
    const hours = difference.hours();
    const minutes = difference.minutes();
    const seconds = difference.seconds();

    const differenceDuration = [
      `${years} y`,
      `${months} mo`,
      `${weeks} w`,
      `${days} d`,
      `${hours} h`,
      `${minutes} m`,
      `${seconds} s`,
    ].find(c => !/^0 (y|mo|w|d|h|m|s)/.test(c));

    content = differenceDuration || '0 s';
  }

  chrome.browserAction.setBadgeText({ text: content });
};

export const setCurrentBadge = async () => {
  const tabInfo = await getCurrentTab();
  const { intervals } = store.getState();
  const tabInterval = intervals.find(({ tab }) => tabInfo.id === tab);

  setBadge(tabInterval?.interval || '');
};
