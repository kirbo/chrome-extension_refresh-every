import { log, getCurrentTab, setCurrentBadge } from '@helpers/common';

chrome.tabs.onCreated.addListener(tab => {
  log('onCreated', tab);
});

chrome.tabs.onUpdated.addListener(
  (
    tabId: number,
    changeInfo: chrome.tabs.TabChangeInfo,
    tab: chrome.tabs.Tab
  ) => {
    if (changeInfo?.status === 'complete') {
      log('onUpdated', tabId, changeInfo, tab);
    }
  }
);

chrome.tabs.onActivated.addListener(async activeInfo => {
  const tabInfo = await getCurrentTab();
  setCurrentBadge();
  log('onActivated', activeInfo, tabInfo);
});

chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
  log('onRemoved', tabId, removeInfo);
});
