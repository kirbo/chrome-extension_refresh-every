import {
  ParentItems,
  ChildItems,
  OnClickItem,
  OnClickInfo,
} from 'refresh-every';

import { log, addMenu } from '@helpers/common';

const contexts = ['page'];
const dummyOnClick = (item: OnClickItem, info: OnClickInfo) => {
  log('item', item);
  log('info', info);
};

const onclick = dummyOnClick;

const subMenu: ChildItems = [
  { title: '10 sec', contexts, onclick },
  { title: '1 min', contexts, onclick },
  { id: 'separator1', type: 'separator' },
  { title: 'Custom', contexts, onclick },
];

const parentMenus: ParentItems = [
  {
    id: 'autoRefreshParent',
    title: 'Refresh every',
    contexts,
    menu: subMenu,
  },
];

addMenu(parentMenus);
