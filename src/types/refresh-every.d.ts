import { Action, Store } from 'redux';

interface GenericObject {
  [key: string]: any;
}

export interface SlothyStore extends Store {
  runSaga: any;
  close: any;
}

export type Intervals = GenericObject;
export type ScopedActions = GenericObject;
export type Actions = GenericObject;
export type ReduxActionPayload = GenericObject;

export interface ReduxAction extends Action {
  payload: ReduxActionPayload;
}

export type ItemType = 'normal' | 'checkbox' | 'radio' | 'separator';

interface iParentItem {
  title?: string;
  id?: string;
  contexts?: string[];
  menu?: ChildItems;
  onclick?: any;
  act?: any;
  parentId?: string;
  type?: ItemType;
}
export type ParentItem = iParentItem;
export type ParentItems = ParentItem[];

interface iChildItem extends iParentItem {}
export type ChildItem = iChildItem;
export type ChildItems = ChildItem[];

export type OnClickItem = {
  editable: boolean;
  frameId: number;
  menuItemId: string;
  pageUrl: string;
  parentMenuItemId: string;
};
export type OnClickInfo = {
  active: boolean;
  audible: boolean;
  autoDiscardable: boolean;
  discarded: boolean;
  favIconUrl: string;
  height: number;
  highlighted: boolean;
  id: number;
  incognito: boolean;
  index: number;
  mutedInfo: {
    muted: boolean;
  };
  pinned: boolean;
  selected: boolean;
  status: string;
  title: string;
  url: string;
  width: number;
  windowId: number;
};

export type ParentId = undefined | string;
export type MenuType = ParentItems | ChildItems;

export type ConvertIdToTitle = {
  id: string;
  title: string;
  parentId: string;
};

export interface RootState {}

export interface RefreshEveryStore extends Store {
  runSaga: any;
  close: any;
}
