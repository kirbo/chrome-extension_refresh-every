import React from 'react';
import ReactDOM, { render as ReactDOMRender } from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from 'webext-redux';

import App from '@views/popup';

if (process.env.NODE_ENV !== 'production') {
  const axe = require('react-axe');
  axe(React, ReactDOM, 2500);
}

const mount = document.getElementById('root');

const render = (Component: any) => {
  const store = new Store();

  store.ready().then(() => {
    ReactDOMRender(
      <Provider store={store}>
        <Component />
      </Provider>,
      mount
    );
  });
};

render(App);
