import { take, call } from 'redux-saga/effects';

import { setCurrentBadge } from '@helpers/common';
import * as actions from './actions';

function* setBadge() {
  setCurrentBadge();
}

export function* watchSetInterval() {
  while (true) {
    yield take(actions.setInterval);
    yield call(setBadge);
  }
}
