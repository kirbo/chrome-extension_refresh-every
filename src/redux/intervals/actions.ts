import { ReduxAction, ScopedActions } from 'refresh-every';
import { scopeActions } from '@helpers/redux';

const SCOPE = 'intervals';

const ACTIONS = {
  SET_INTERVAL: 'SET-INTERVAL',
  REMOVE_INTERVAL: 'REMOVE-INTERVAL',
};

export const ACTION_TYPES = scopeActions(SCOPE, ACTIONS) as ScopedActions;

type payload = {
  tab: string;
  interval: number;
};

export const setInterval = ({ tab, interval }: payload): ReduxAction => ({
  type: ACTION_TYPES.SET_INTERVAL,
  payload: { tab, interval },
});

export const removeInterval = ({ tab, interval }: payload): ReduxAction => ({
  type: ACTION_TYPES.REMOVE_INTERVAL,
  payload: { tab, interval },
});
