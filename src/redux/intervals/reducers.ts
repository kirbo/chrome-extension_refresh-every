import { ReduxAction } from 'refresh-every';

import { ACTION_TYPES } from './actions';
import initialState from './initialState';

export default (state: any = initialState, action: ReduxAction) => {
  const { type, payload } = action;

  const types = {
    [ACTION_TYPES.SET_INTERVAL]: (): any => {
      if (state.find(({ tab }) => payload.tab === tab)) {
        return state.map(interval =>
          payload.tab === interval.tab ? payload : interval
        );
      } else {
        return [...state, payload];
      }
    },
    [ACTION_TYPES.REMOVE_INTERVAL]: (): any =>
      state.filter(({ tab }) => payload.tab !== tab),
  };

  return (types[type] ? types[type]() : state) as any;
};
