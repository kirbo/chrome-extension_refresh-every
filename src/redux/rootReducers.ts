import { combineReducers } from 'redux';

import intervals from './intervals/reducers';

export const rootReducer = combineReducers({
  intervals,
});
