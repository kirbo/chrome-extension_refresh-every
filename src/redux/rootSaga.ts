import { all } from 'redux-saga/effects';

import { watchSetInterval } from './intervals/sagas';

export default function* root() {
  yield all([watchSetInterval()]);
}
