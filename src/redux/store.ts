///<reference types="webpack-env" />

import { RefreshEveryStore } from 'refresh-every';

import { wrapStore } from 'webext-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';
import sagaMonitor from '@redux-saga/simple-saga-monitor';

import { rootReducer } from './rootReducers';
import rootSaga from './rootSaga';

const configureStore = (initialState: object = {}) => {
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

  const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(sagaMiddleware))
  ) as RefreshEveryStore;

  wrapStore(store);

  if (module.hot) {
    module.hot.accept();

    module.hot.accept('./rootReducer.ts', () => {
      const nextRootReducerMain = require('./rootReducer').rootReducerMain;
      store.replaceReducer(nextRootReducerMain);
    });

    module.hot.accept('./rootSaga.ts', () => {
      const nextRootSaga = require('./rootSaga');
      store.runSaga(nextRootSaga);
    });
  }

  store.runSaga = sagaMiddleware.run;
  store.runSaga(rootSaga);
  store.close = () => store.dispatch(END);
  return store;
};

export default configureStore();
